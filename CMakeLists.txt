cmake_minimum_required(VERSION 2.4.7)

if(COMMAND cmake_policy)
	cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)


PROJECT(qhy5ii_ccd CXX C)

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules/")
set(BIN_INSTALL_DIR "${CMAKE_INSTALL_PREFIX}/bin")

find_package(CFITSIO REQUIRED)
find_package(INDI REQUIRED)
find_package(ZLIB REQUIRED)

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/config.h.cmake ${CMAKE_CURRENT_BINARY_DIR}/config.h )

include_directories( ${CMAKE_CURRENT_BINARY_DIR})
include_directories( ${CMAKE_SOURCE_DIR})
include_directories ("${PROJECT_SOURCE_DIR}/qhyccd")
include_directories( ${INDI_INCLUDE_DIR})
include_directories( ${CFITSIO_INCLUDE_DIR})


add_subdirectory (qhyccd) 

############# QHY5II CCD ###############
if (CFITSIO_FOUND)

set(qhy5iiccd_SRCS
	${CMAKE_SOURCE_DIR}/qhy5ii_ccd.cpp
)

add_executable(indi_qhy5ii_ccd ${qhy5iiccd_SRCS})

target_link_libraries(indi_qhy5ii_ccd qhyccd ${INDI_DRIVER_LIBRARIES} ${CFITSIO_LIBRARIES} m z)

install(TARGETS indi_qhy5ii_ccd RUNTIME DESTINATION bin)

endif (CFITSIO_FOUND)

install(FILES indi_qhy5ii_ccd.xml DESTINATION ${INDI_DATA_DIR})
