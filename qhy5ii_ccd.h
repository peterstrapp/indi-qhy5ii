/*
 QHY5L-II-C driver
 Copyright (C) 2014 Peter Strapp (peter@strapp.co.uk)

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 */

#ifndef QHY5II_CCD_H
#define QHY5II_CCD_H

#include <indiccd.h>
#include <iostream>

using namespace std;

#define DEVICE struct usb_device *

class QHY5IICCD: public INDI::CCD {
public:

  QHY5IICCD(const char *name);
  virtual ~QHY5IICCD();

  const char *getDefaultName();

  bool initProperties();
  void ISGetProperties(const char *dev);
  bool updateProperties();

  bool Connect();
  bool Disconnect();

  bool StartExposure(float duration);
  bool AbortExposure();

  virtual bool ISNewSwitch(const char *dev, const char *name, ISState *states, char *names[], int n);
  virtual bool ISNewNumber (const char *dev, const char *name, double values[], char *names[], int n);

protected:

  void TimerHit();
  virtual bool UpdateCCDFrame(int x, int y, int w, int h);
  virtual bool UpdateCCDBin(int binx, int biny);
  virtual void addFITSKeywords(fitsfile *fptr, CCDChip *targetChip);
  virtual bool UpdateCCDFrameType(CCDChip::CCD_FRAME fType);

  // Guide Port
  virtual bool GuideNorth(float);
  virtual bool GuideSouth(float);
  virtual bool GuideEast(float);
  virtual bool GuideWest(float);

private:
  DEVICE device;
  char name[32];

  ISwitch ResetS[1];
  ISwitchVectorProperty ResetSP;

  INumber GainN[1];
  INumberVectorProperty GainNP;

  INumber OffsetN[1];
  INumberVectorProperty OffsetNP;

  INumber UsbTrafficN[1];
  INumberVectorProperty UsbTrafficNP;

  double ccdTemp;
  double minDuration;

  int timerID;

  CCDChip::CCD_FRAME imageFrameType;

  struct timeval ExpStart;

  float ExposureRequest;
  float TemperatureRequest;

  float CalcTimeLeft();
  int grabImage();
  bool setupParams();
  void resetFrame();

  bool sim;

  friend void ::ISGetProperties(const char *dev);
  friend void ::ISNewSwitch(const char *dev, const char *name, ISState *states, char *names[], int num);
  friend void ::ISNewText(const char *dev, const char *name, char *texts[], char *names[], int num);
  friend void ::ISNewNumber(const char *dev, const char *name, double values[], char *names[], int num);
  friend void ::ISNewBLOB(const char *dev, const char *name, int sizes[], int blobsizes[], char *blobs[], char *formats[], char *names[], int n);
};

#endif // QHY5II_CCD_H
