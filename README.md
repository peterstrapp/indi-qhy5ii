QHY5L-II INDI Driver
====

This package provides an INDI CCD driver for the QHY5II camera. Currently tested on a QHY5L-II-C.

Version control
-----

```sh
git clone https://peppertarts@bitbucket.org/peppertarts/indi-qhy5ii.git
```

Build
-----

```sh
sudo apt-get update
sudo apt-get install -y lfxload libcfitsio3-dev libopencv-dev libnova-dev

mkdir indi-qhy5ii_build
cd indi-qhy5ii_build/
cmake -DCMAKE_INSTALL_PREFIX=/usr . ../indi-qhy5ii/
make
sudo make install
```

Firmware Installation
---------------------

```sh
sudo apt-get install fxload
sudo mkdir -p /etc/qhyccd
sudo cp QHY5II.HEX /etc/qhyccd/
sudo cp 85-qhyccd.rules /etc/udev/rules.d/
```
